const http          = require('http')
const url           = require('url');
const Promise       = require('bluebird');
const redis         = require('redis');
const Raven         = require('raven');
const geoip         = require('geoip-lite');
const MongoClient   = Promise.promisifyAll(require('mongodb').MongoClient);

const config        = require('./config/config');
const revenue       = require('./config/revenue.json');

Promise.promisifyAll(redis.RedisClient.prototype);

Raven.config(config.raven).install();

let _db;
let _client;

//
// ─── GEOIP ──────────────────────────────────────────────────────────────────────
//

function getCountryCode (request) {

    let rt          = {};

    const ip        = request.headers['x-real-ip'];
    const geo       = geoip.lookup(ip);

    if ( typeof geo != 'undefined' && geo && geo.country) {
        const countryCode   = geo.country;

        for (let revenueKey in revenue) {
            if ( countryCode == revenueKey ) {
                rt = { code:countryCode, revenue: revenue[revenueKey]};
                break;
            }
            rt = { code:countryCode, revenue: '0.1'}
        }
    
        return rt;
    }
    return rt = { code:'unresolved', revenue: 'unresolved' };

}

//
// ─── HELPER FUNCTIONS ───────────────────────────────────────────────────────────
//


function returnPid (pid) {

    let rt = {}
    if ( typeof pid != 'undefined' && pid ) {

        const splittedPID   = pid.split('_');
        const pidPartner    = splittedPID[0];
        const pidUnique     = splittedPID[1] || 'undefined';
        return rt = { pidPartner:pidPartner, pidUnique: pidUnique };

    }

    return rt = { pidPartner:'undefined', pidUnique: 'undefined' };

}

//
// ─── REQUEST HANDLER ────────────────────────────────────────────────────────────
//

async function requestHandler (req, res) {
    try {

        const { to, partner, click, pid } = url.parse(req.url, true).query;

        if ( to && partner && click ) {
            // stats
            await _client.hincrbyAsync('stats', 'traffic-server', 1);
            // obtain domen from mongodb
            const collectionExt     = _db.collection('extensions');
            const collectionAcc     = _db.collection('accounts');
            const response          = await collectionExt.findOne({name:to, status:2});
            if ( !response ) return res.end();
            const accountId         = response.owner;
            const account           = await collectionAcc.findOne({_id:accountId});
            const domen             = account.domen;

            const { code, revenue }         = getCountryCode(req);
            const { pidPartner, pidUnique } = returnPid(pid);

            if (pidPartner) {
                await _client.hincrbyAsync(`partner-integration:${pidPartner}`, 'click_count', 1);
            }

            await _client.hmsetAsync(`postback-integration:${click}`, ['date', new Date(), 'partner', partner, 'clickID', click, 'webmasterPID', pidPartner, 'sub3', pidUnique, 'country', code, 'revenue', revenue]);

            res.writeHead(302, {
                'Location': `${domen}/${to}/?partner=${partner}&click=${click}`
            });
            return res.end();

        }

        res.end();

    } catch ( err ) {

        Raven.captureException(err);
        res.end();

    }
}


//
// ─── SERVER START ───────────────────────────────────────────────────────────────
//

const server = http.createServer(requestHandler)

MongoClient.connect(`${config.mongo.ip}/${config.mongo.name}`)
    .then( (client) => {
        _db = client.db(config.mongo.name);
    })
    .then( () => {
        _client = redis.createClient({host:config.redis.host, port:config.redis.port});
    })
    .then( () => {
        server.listen(config.port);
    })
    .catch( ( err ) => {
        Raven.captureException(err);
    })